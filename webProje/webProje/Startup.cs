﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webProje.Startup))]
namespace webProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
